import axios from 'axios';

export const API_CONFIG = {
  HEADERS: {
    "Content-Type": "application/json",
  },

  BASE_URL_SCHEMA: '/tmf-api'
}
// tmf-api/workTicketManagement/v1/workTicket/<ID_WO>?@baseType=workOrder
export const API_MANAGER = {
  PATCH_UPDATE_WORKORDER: {
    url: API_CONFIG.BASE_URL_SCHEMA + '/workTicketManagement/v1/workTicket/{workOrderId}?@baseType=workOrder',
    method: 'PATCH'
  },
  GET_ALL_THRESHOLD: {
    url: API_CONFIG.BASE_URL_SCHEMA + '/649/thresholdRule',
    method: 'GET'
  },
  PATCH_UPDATE_THRESHOLD: {
    url: API_CONFIG.BASE_URL_SCHEMA + '/649/thresholdRule/{thresholdId}',
    method: 'PATCH'
  },

}

export type URL_TYPES = 'PATCH_UPDATE_WORKORDER'
  | 'GET_ALL_THRESHOLD' | 'PATCH_UPDATE_THRESHOLD';


axios.interceptors.request.use((request) => {
  // console.log('INTERCEPTOR: ', request)

  return request;
});

axios.defaults.headers = API_CONFIG.HEADERS;
export default axios;