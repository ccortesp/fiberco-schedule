export const lightBlue ="#3682ff";
export const blue = "#3984ff";
export const darkBlue = "#0058cb";
export const darkerBlue = "#303846";
export const red = "#eb295f";
export const lowGrey = "#f4f4f4";
export const midGrey = "#e4e4e4";
export const grey = "#5f5f5f";
export const black = "#212529";


export default {
  lightBlue,
  blue,
  darkBlue,
  darkerBlue,
  red,
  lowGrey,
  midGrey,
  grey,
  black

}