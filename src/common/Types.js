export type Props = {
  classes: object,
  index: number
}
export type Availability = {
  days: [],
  hours: []
}
export type Technician = {
  authID: string,
  email: string,
  firstName: string,
  id: string,
  lastName: string,
  role: string,
  status: string,
  availability: Availability,
  scheduled: Date[]
}

export type Measurement = {
  id: string, //show readonly
  name: string,//show readonly
  description: string, //show readonly
  measurementType: string, //show readonly
  measurementUnit: string, //show readonly
}

export type SimpleThresholdRuleOption = "GT"
  | "GE" | "EQ" | "NEQ" | "0" | "LE" | "LT";

export type SimpleThresholdRule = {
  conformanceTargetUpper: SimpleThresholdRuleOption,
  conformanceTargetLower: SimpleThresholdRuleOption,
  conformanceComparatorUpper: SimpleThresholdRuleOption,
  conformanceComparatorLower: SimpleThresholdRuleOption,

}

export type Threshold = {
  id: string,
  thresholdRuleName: string,
  measurement: Measurement,
  simpleThresholdRule: SimpleThresholdRule

}
