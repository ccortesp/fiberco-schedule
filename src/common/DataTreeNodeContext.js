import React, { createContext, useState } from 'react';
import { Resource } from './Types';


export const DataTreeNodeContext = createContext();

const DataTreeNodeContextProvider = ({ children }) => {


  const [nodes, updateNodes] = useState([]);
  const [selectedNode, setSelectedNode] = useState({ id: -1 });


  const setNodes = (newNodes) => {
    updateNodes(newNodes.map(n => {
      return { ...n }
    }))
  }

  const values = {
    nodes,
    setNodes,
    selectedNode,
    setSelectedNode
  }

  return (
    <DataTreeNodeContext.Provider
      value={values}>

      {children}

    </DataTreeNodeContext.Provider>
  );
}

export default DataTreeNodeContextProvider;