import {
  ViewList as ScheduleIcon,
  DynamicFeed as ThresholdIcon
} from "@material-ui/icons";

import SchedulePage from "../pages/Schedule";
import ThresholdPage from "../pages/Threshold";


export const links = [
  {
    path: '/schedule',
    component: SchedulePage,
    name: 'Schedule',
    index: 0,
    show: true,
    icon: ScheduleIcon

  },
  {
    path: '/threshold',
    component: ThresholdPage,
    name: 'Threshold',
    index: 1,
    show: true,
    icon: ThresholdIcon

  },

];
