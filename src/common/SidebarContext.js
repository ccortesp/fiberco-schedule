import React, { createContext, useState } from 'react';

export const SidebarContext = createContext();

const SidebarContextProvider = ({ children }) => {


  const [isCollapsed, setCollapsed] = useState(true);
  const [selectedPage, setSelectedPage] = useState(0);

  const values = {
    isCollapsed,
    setCollapsed,
    selectedPage,
    setSelectedPage
  }

  return (
    <SidebarContext.Provider
      value={values}>

      {children}

    </SidebarContext.Provider>
  );
}

export default SidebarContextProvider;