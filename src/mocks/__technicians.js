export const TECHS = `[
  {
      "authID": "juan.camilo.bernal.soto@everis.com",
      "email": "juan.camilo.bernal.soto@everis.com",
      "firstName": "Juan Camilo",
      "id": "176093659137",
      "lastName": "Bernal Soto",
      "role": "ADMIN",
      "status": "ACTIVE",
      "availability": {
          "days": [
              1,
              2,
              3,
              4,
              5
          ],
          "hours": [
              7,
              8,
              9,
              10,
              12,
              15
          ]
      },
      "scheduled": []
  },
  {
      "authID": "diego.andres.restrepo.vera@everis.com",
      "email": "diego.andres.restrepo.vera@everis.com",
      "firstName": "Diego",
      "id": "176093659138",
      "lastName": "Restrepo",
      "role": "ADMIN",
      "status": "ACTIVE",
      "availability": {
          "days": [
              1,
              2,
              3,
              5
          ],
          "hours": [
              10,
              12,
              13,
              14,
              15
          ]
      },
      "scheduled": []
  },
  {
      "authID": "carlos.alberto.lugo.montero@everis.com",
      "email": "carlos.alberto.lugo.montero@everis.com",
      "firstName": "Carlos",
      "id": "176093659139",
      "lastName": "Lugo",
      "role": "ADMIN",
      "status": "ACTIVE",
      "availability": {
          "days": [
              1,
              2,
              3,
              5
          ],
          "hours": [
              10,
              12,
              15
          ]
      },
      "scheduled": []
  },
  {
      "authID": "carlos.anibal.cortes.palacio@everis.com",
      "email": "carlos.anibal.cortes.palacio@everis.com",
      "firstName": "Carlos",
      "id": "176093659140",
      "lastName": "Cortes",
      "role": "ADMIN",
      "status": "ACTIVE",
      "availability": {
          "days": [
              1,
              5
          ],
          "hours": [
              7,
              8,
              9,
              10,
              11
          ]
      },
      "scheduled": []
  }
]`