import React, { useContext, useEffect, useState } from 'react';
import classNames from 'classnames';
import { makeStyles, Snackbar, Button } from '@material-ui/core';
import LoadingProgress from '../components/LoadingProgress';
import ThresholdCard from '../components/ThresholdCard';
import { black, darkBlue } from '../common/colors';
import { SidebarContext } from '../common/SidebarContext';
import { Props, Threshold } from '../common/Types';
import { createRequest } from '../utils/RequestProvider';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { Link } from 'react-router-dom';
import { v1 as uuid } from 'uuid';

const useStyles = makeStyles({
    root: {

    },
    content: {
        padding: "24px",
        overflow: "auto",
    },
    menu: {
        boxShadow: "0 0 12px -2.5px rgba(0, 0, 0, 0.21)",
        backgroundColor: "white",
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        paddingLeft: "15px",
        paddingRight: "15px",
        "& .options": {
            display: "flex",
            alignItems: "center",

        }
    },
    option: {
        padding: "20px",
        textDecoration: "none",
        color: black,
        "&.visited, &:active": {
            textEmphasis: "none",
            color: black,
        },
        "&.selected": {
            borderBottom: `5px solid ${darkBlue}`,
            color: darkBlue,
            fontWeight: "bold"
        },
    }

});


const ThresholdPage = (props: Props) => {


    const [loading, setLoading] = useState(true);
    const [open, setOpen] = useState(false);
    const [thresholds, setThresholds] = useState([])

    const classes = useStyles();

    const { setSelectedPage } = useContext(SidebarContext);


    useEffect(() => {
        setSelectedPage(props.index);
        getThresholds();
    }, [])


    const getThresholds = () => {
        createRequest("GET_ALL_THRESHOLD", null, null,
            ({ data }) => {
                setThresholds(data);
                setLoading(false);

            },
            (err) => {
                setLoading(false);
            }
        )
    }

    const handleOnSaveElement = (threshold: Threshold) => {
        setLoading(true);
        createRequest(
            "PATCH_UPDATE_THRESHOLD",
            threshold,
            { "thresholdId": threshold.id },
            (res) => {
                getThresholds();
                setOpen(true);
            },
            (err) => {
                setLoading(false);
            }
        )
    }



    const handleSnackbarClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpen(false);
    };

    return (

        <div className={classNames(props.classes.page, classes.root)}>


            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                open={open}
                autoHideDuration={4000}
                onClose={handleSnackbarClose}
                message="Threshold updated successfully"
                action={
                    <React.Fragment>
                        <IconButton
                            size="small"
                            aria-label="close"
                            color="inherit"
                            onClick={handleSnackbarClose}>
                            <CloseIcon fontSize="small" />
                        </IconButton>
                    </React.Fragment>
                }
            />


            <div className={classes.menu}>
                <div className={classNames({ options: true })}>

                    <Link
                        className={
                            classNames(classes.option, { selected: true })
                        }
                    >
                        Threshold Rules
                    </Link>
                </div>
            </div>

            <div className={classes.content}>
                <div className={classes.contentHeader}>
                    <div className={classes.contentTitle}>
                        {
                            !loading ?
                                thresholds.map((item: Threshold) => {
                                    return <ThresholdCard
                                        key={uuid()}
                                        item={item}
                                        onSave={handleOnSaveElement} />
                                }) :
                                <LoadingProgress />
                        }

                    </div>
                </div>

            </div>
        </div >
    );
}

export default ThresholdPage;