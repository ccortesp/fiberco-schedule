import React, { useContext, useEffect, useState } from 'react';
import classNames from 'classnames';
import { makeStyles, Snackbar, Button } from '@material-ui/core';
import LoadingProgress from '../components/LoadingProgress';
import TechnicianCard from '../components/TechnicianCard';
import { black, darkBlue } from '../common/colors';
import { SidebarContext } from '../common/SidebarContext';
import { Props, Technician } from '../common/Types';
import { Link, useLocation, Redirect } from 'react-router-dom';
import { createRequest } from '../utils/RequestProvider';
import { TECHS } from '../mocks/__technicians';
import AssignModal from '../components/AssignModal';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { format } from 'date-fns'

const useStyles = makeStyles({
  root: {

  },
  content: {
    padding: "24px",
    overflow: "auto",
  },
  menu: {
    boxShadow: "0 0 12px -2.5px rgba(0, 0, 0, 0.21)",
    backgroundColor: "white",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    paddingLeft: "15px",
    paddingRight: "15px",
    "& .options": {
      display: "flex",
      alignItems: "center",

    }
  },
  option: {
    padding: "20px",
    textDecoration: "none",
    color: black,
    "&.visited, &:active": {
      textEmphasis: "none",
      color: black,
    },
    "&.selected": {
      borderBottom: `5px solid ${darkBlue}`,
      color: darkBlue,
      fontWeight: "bold"
    },
  }

});

const SchedulePage = (props: Props) => {


  const [loading, setLoading] = useState(true);
  const [open, setOpen] = useState(false);
  const [resourceTypes, setResourceTypes] = useState([]);

  const [selectedItem, setSelectedItem] = useState(null);

  const [technicians, setTechnicians] = useState([]);
  const [workOrderId, setWorkOrderId] = useState('');

  const classes = useStyles();

  const { setSelectedPage } = useContext(SidebarContext);

  const params = new URLSearchParams(useLocation().search);

  useEffect(() => {
    setSelectedPage(0);
    loadTechnicians();

    setWorkOrderId(params.get("workOrder"));
    setLoading(false);
  }, [])

  const loadTechnicians = () => {
    let techs = JSON.parse(localStorage.getItem("techs"));

    if (techs === null) {
      techs = JSON.parse(TECHS);
    }


    setTechnicians(techs.filter((tech: Technician) => {
      if (tech.availability.hours.length > 0) {
        return tech;
      }
    }));

  }


  const onItemSelectForAssign = (item: Technician, date) => {
    setLoading(true);

    const data = {
      // 2020-09-04T22:26
      "characteristic": [
        {
          "name": "Scheduling date",
          "valueType": "string",
          "value": `${format(date, "dd-MM-yyyy HH:mm")}`,
          "@baseType": "propertyType"
        },
        {
          "name": "Technician",
          "valueType": "string",
          "value": `${item.firstName} ${item.lastName}`,
          "@baseType": "propertyType"
        },
        {
          "name": "idTechnician",
          "valueType": "string",
          "value": `${item.id}`,
          "@baseType": "propertyType"
        }
      ]

    }

    createRequest("PATCH_UPDATE_WORKORDER",
      data,
      { "workOrderId": workOrderId },
      (res) => {
        console.log(res);
        localStorage.setItem("techs", JSON.stringify(technicians));
        loadTechnicians();
        setLoading(false)
        setOpen(true);
      },
      (err) => {
        console.log(err);
        setLoading(false)
        setOpen(true);
      })

  }


  const handleSnackbarClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    window.open("https://fb-test.thesymphony.cloud/workorders/search?workorder=" + workOrderId);
    setOpen(false);
  };

  return (

    <div className={classNames(props.classes.page, classes.root)}>


      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        open={open}
        autoHideDuration={2000}
        onClose={handleSnackbarClose}
        message="Work order updated successfully"
        action={
          <React.Fragment>
            <IconButton
              size="small"
              aria-label="close"
              color="inherit"
              onClick={handleSnackbarClose}>
              <CloseIcon fontSize="small" />
            </IconButton>
          </React.Fragment>
        }
      />


      <div className={classes.menu}>
        <div className={classNames({ options: true })}>

          <Link
            className={
              classNames(classes.option, { selected: true })
            }
          >
            Technicians
          </Link>
        </div>
      </div>

      <div className={classes.content}>
        <div className={classes.contentHeader}>
          <div className={classes.contentTitle}>
            {
              !loading ?
                technicians.map((item: Technician) => {
                  return <TechnicianCard
                    item={item}
                    onSelect={onItemSelectForAssign} />
                }) :
                <LoadingProgress />
            }

          </div>
        </div>

      </div>
    </div >
  );
}

export default SchedulePage;