import React, { useContext } from "react";
import { v1 as uuid } from 'uuid';
import {
  ArrowBackIosSharp as BackIcon,
  ArrowForwardIosSharp as NextIcon,
} from "@material-ui/icons";
import { Link } from "react-router-dom";
import { SidebarContext } from "../common/SidebarContext";
import SchedulePage from "../pages/Schedule";
import classNames from 'classnames';
import { darkerBlue, blue } from "../common/colors";
import { makeStyles } from "@material-ui/core";
import { links } from "../common/routes";


const useStyles = makeStyles({
  sidebar: {
    width: "150px !important",
    minWidth: "150px !important",
    height: "100vh",
    display: "flex",
    padding: "20px 0px 20px 0px",
    position: "relative",
    boxShadow: "1px 0px 0px 0px rgba(0, 0, 0, 0.1)",
    alignItems: "center",
    flexDirection: "column",
    backgroundColor: darkerBlue,
    "&.collapsed": {
      width: "70px !important",
      minWidth: "70px !important",
    }
  },
  sidebarLinks: {
    width: "100%",
    display: "flex",
    flexGrow: 1,
    alignItems: "center",
    paddingTop: "40px",
    flexDirection: "column",
  },
  sidebarLink: {
    width: "100%",
    display: "flex",
    padding: "15px",
    justifyContent: "left",
    color: "white",
    alignItems: "center",
    textDecoration: "none !important",
    "&.selected": {
      backgroundColor: blue,
      color: "white"
    },
    "&:hover": {
      color: "white"
    },
    "&:active": {
      textDecoration: "none !important"

    },
    "&:visited": {
      textDecoration: "none !important"
    },
    "&.collapsed": {
      justifyContent: "center"
    }
  },
  sidebarItem: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
  },
  sidebarItemName: {
    marginLeft: '10px',
    "&.collapsed": {
      display: "none"
    }
  },
  sidebarIcon: {
    fontSize: '20px'
  }
})


const Sidebar = () => {

  const {
    isCollapsed,
    selectedPage,
    setSelectedPage,
    setCollapsed
  } = useContext(SidebarContext);

  const classes = useStyles();




  const sidebarItemNameClasses = classNames(
    classes.sidebarItemName,
    { collapsed: isCollapsed }
  );

  return (
    <div className={classNames(classes.sidebar, { collapsed: isCollapsed })}>
      <div className={classNames(classes.sidebarLinks, classes.sidebarItem)}>

        {
          links.map((link) => {
            const isSelected = selectedPage === link.index;
            const linkClasses = classNames(
              classes.sidebarLink,
              { selected: isSelected },
              { collapsed: isCollapsed }
            )

            return (
              <Link
                key={uuid()}
                className={linkClasses}
                onClick={() => setSelectedPage(link.index)}
                to={{
                  pathname: link.path,
                  state: { fromDashboard: true }
                }}
              >
                <link.icon className={classes.sidebarIcon} />
                <span className={sidebarItemNameClasses}>{link.name}</span>
              </Link>
            )
          })
        }

      </div>

      <div className={classes.sidebarItem}>
        <span
          className={classes.sidebarLink}
          onClick={() => {
            setCollapsed(!isCollapsed);
          }}
        >

          {
            isCollapsed
              ? <NextIcon className={classes.sidebarIcon} />
              : <BackIcon className={classes.sidebarIcon} />
          }
          <span className={sidebarItemNameClasses}>Collapse</span>
        </span>
      </div>
    </div >
  );
};

export default Sidebar;
