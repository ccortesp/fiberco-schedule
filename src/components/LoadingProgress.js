import React from 'react';
import { CircularProgress } from '@material-ui/core';

const styles = {
  progress: { margin: '15px auto', display: 'block' },
  container: { width: '100%' }
};

const LoadingProgress = () => {
  return (
    <div style={styles.container}>
      <CircularProgress style={styles.progress} />
    </div>
  );
}

export default LoadingProgress;