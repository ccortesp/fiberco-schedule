import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core';
import { blue, grey, darkBlue, lightBlue, lowGrey, midGrey } from '../common/colors';
import { DevicesOther as DeviceIcon } from '@material-ui/icons';
import { Technician, Availability } from '../common/Types';
import classNames from 'classnames';
import DatePicker from 'react-datepicker';
import { setHours, setMinutes, getDay, getHours } from 'date-fns';
import "react-datepicker/dist/react-datepicker.css";


const useStyles = makeStyles({
  cardContainer: {
    display: "flex",
    alignItems: "center",
    marginTop: "15px",
    borderRadius: "2px",
    boxShadow: "0 0 12px -2.5px rgba(0, 0, 0, 0.21)",
    backgroundColor: "white",
    padding: "15px",
  },
  card: {
    display: "flex",
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  itemName: {
    fontWeight: "bold",
    fontSize: "14px",
    width: "200px",
  },
  itemCount: {
    flexGrow: 1,
    display: "flex",
    justifyContent: "center",
    fontSize: "14px",
  },
  icon: {
    background: blue,
    color: "white",
    border: `1px solid ${darkBlue}`,
    padding: "8px",
    borderRadius: "100%",
    width: "40px",
    height: "40px",
    textAlign: "center",
    justifyContent: "center",
    alignItems: "center",
    marginRight: "20px",
  },
  availability: {
    display: "flex",
    flexDirection: "column"
  },
  btn: {
    border: "none",
    borderRadius: "5px",
    color: "white",
    height: "30px",
    padding: "0px 15px",
    fontSize: "13px",
    cursor: "pointer",
    fontWeight: "bold",
    "&.btnPrimary": {
      backgroundColor: darkBlue,
      "&:hover": {
        backgroundColor: blue
      },
      "&:disabled,&[disabled]": {
        backgroundColor: midGrey
      }
    },
    "&.btnSecondary": {
      backgroundColor: grey,
      color: "white",
      "&:hover": {
        backgroundColor: grey,
      }
    }
  },
  options: {
    display: "flex",
    flexDirection: "row",
    "& button": {
      margin: "0 2px 0 2px"
    }
  },
  availableTime: {
    color: "green"
  },
  noAvailableTime: {
    color: "red"
  }
});

type Props = {
  item: Technician,
  icon: DeviceIcon,
  onSelect: func
}



const TechnicianCard = (props: Props) => {
  const { item, icon } = props;
  const classes = useStyles();
  const [selectedDate, handleDateChange] = useState(null);
  const [excludedTimes, setExcludedTimes] = useState([]);
  const [disabled, setDisabled] = useState(true);

  useEffect(() => {
    getExcludedTimes();
  }, [])

  useEffect(() => {

    console.log(getHours(selectedDate));
    setDisabled(
      selectedDate === null
      || !item.availability.hours.includes(getHours(selectedDate))
    );

  }, [selectedDate])

  const getExcludedTimes = () => {
    const tmp = [];

    for (let i = 0; i < 24; i++) {
      if (!item.availability.hours.includes(i)) {
        tmp.push(setHours(setMinutes(new Date(), 0), i));
      }
    }
    setExcludedTimes(tmp);

  }


  const getExcludedDays = (date) => {
    const day = getDay(date);
    return day !== 0 && day !== 6 && item.availability.days.includes(day);
  };


  const handleColor = time => {
    return time.getHours() > 12 ? "availableTime" : "noAvailableTime";
  };

  const handleAssignSchedule = () => {

    const hour = getHours(selectedDate);

    item.availability.hours.forEach((h, i) => {
      if (h === hour) {
        item.availability.hours.splice(i, 1);
      }
    })
    item.scheduled.push(selectedDate);

    props.onSelect(item, selectedDate);
  }

  const getExcludedDates = () => {

    return item.scheduled.map(date => new Date(date));
  }
  const ExampleCustomInput = ({ value, onClick }) => (
    <button
      className={classNames(classes.btn, { btnSecondary: true })}
      onClick={onClick}>
      {!value ? "Select date" : value}
    </button>
  );



  return (
    <div className={classes.cardContainer}>
      <div className={classes.card}>
        <DeviceIcon className={classes.icon} />
        <span className={classes.itemName}>
          {item.firstName + " " + item.lastName}
        </span>
        <span className={classes.itemName}>
          {item.email}
        </span>

        <div className={classes.itemCount}>



        </div>

        <div className={classes.options}>
          <DatePicker
            minDate={new Date()}
            selected={selectedDate}
            onChange={handleDateChange}
            showTimeSelect
            timeFormat="p"
            timeIntervals={60}
            dateFormat="Pp"
            filterDate={getExcludedDays}
            excludeTimes={excludedTimes}
            customInput={<ExampleCustomInput />}
            popperPlacement="top-end"
            placeholderText="Select date"
            excludeDates={getExcludedDates()}
            timeClassName={handleColor} />
          <span> </span>
          <button
            disabled={disabled}
            className={classNames(classes.btn, { btnPrimary: true })}
            onClick={handleAssignSchedule}
          >
            Assign
          </button>
        </div>

      </div>
    </div>
  );
}

export default TechnicianCard;