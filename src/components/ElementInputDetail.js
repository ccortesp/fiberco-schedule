
import React, { useState, useEffect } from 'react';
import classNames from 'classnames';
import { makeStyles, Select, MenuItem } from '@material-ui/core';
import { darkerBlue } from '../common/colors';
import { v1 as uuid } from 'uuid';
const useStyles = makeStyles({
  row: {
    display: "flex",
    flexDirection: "row !important",
    alignItems: "center",
    maxWidth: "100%",
    marginBottom: "20px",

    "& label": {
      fontSize: "15px",
      width: "100%",
      flexGrow: 1,
      " &:first-child": {
        fontWeight: "bold",
      },
      "&:last-child": {
        textAlign: "right",
      }
    },

    "& .input": {
      textAlign: "right !important",
      width: "250px",
      "&.focused": {
        border: `solid 1px ${darkerBlue}`,
      },
      "&.changed": {
        border: "solid 1px red",
      }
    }
  }
});

type Props = {
  value: string,
  name: string,
  onInputChange: func
}

const ElementInputDetail = (props: Props) => {
  const classes = useStyles();

  const { id, name, onInputChange } = props;
  const [value, setValue] = useState('');

  useEffect(() => {
    setValue(props.value)
  }, [])


  const handleInputChange = (event) => {
    setValue(event.target.value)
    onInputChange(name, event.target.value);

  }



  return <div className={classes.row}>
    <label>{name}:</label>
    <input
      type='text'
      name={name}
      className={classNames({ input: true, text: true })}
      value={value}
      onChange={e => handleInputChange(e)} />

  </div>
}

export default ElementInputDetail;