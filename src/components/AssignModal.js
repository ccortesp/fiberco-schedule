
import React from 'react';
import { Modal, makeStyles } from '@material-ui/core';
import { grey } from '@material-ui/core/colors';
import Backdrop from '@material-ui/core/Backdrop';
import LoadingProgress from './LoadingProgress';

const useStyles = makeStyles({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: "#FFF",
    padding: "10px"
  },
})

const AssignModal = (props) => {

  const classes = useStyles();


  return <Modal
    className={classes.modal}
    open={props.open}
    closeAfterTransition
    BackdropComponent={Backdrop}
    aria-labelledby="transition-modal-title"
    aria-describedby="transition-modal-description"
    onClose={props.onClose}
    disableAutoFocus={true}
    BackdropProps={{
      timeout: 500,
    }}
  >

    <div className={classes.paper}>
      <LoadingProgress />
    </div>
  </Modal>

}

export default AssignModal;
