
import React, { useState, useEffect } from 'react';
import classNames from 'classnames';
import { makeStyles, Select, MenuItem } from '@material-ui/core';
import { darkerBlue } from '../common/colors';
import { v1 as uuid } from 'uuid';
const useStyles = makeStyles({
  row: {
    display: "flex",
    flexDirection: "row !important",
    alignItems: "center",
    maxWidth: "100%",
    marginBottom: "20px",

    "& label": {
      fontSize: "15px",
      width: "100%",
      flexGrow: 1,
      " &:first-child": {
        fontWeight: "bold",
      },
      "&:last-child": {
        textAlign: "right",
      }
    },

    "& .input": {
      textAlign: "right !important",
      width: "250px",
      "&.focused": {
        border: `solid 1px ${darkerBlue}`,
      },
      "&.changed": {
        border: "solid 1px red",
      }
    }
  }
});

type Props = {
  name: string,
  value: any,
  onInputChange: func
}

const ElementInputDetail = (props: Props) => {
  const classes = useStyles();

  const { id, name, onInputChange } = props;

  const [value, setValue] = useState('');

  useEffect(() => {
    setValue(props.value)
  }, [])


  const handleInputChange = (event) => {

    setValue(event.target.value)
    onInputChange(name, event.target.value);
  }

  const selectionTypes = [
    {
      value: "GT",
      label: "GT - Greater than"
    },
    {
      value: "GE",
      label: "GE - Greater or equal"
    },
    {
      value: "EQ",
      label: "EQ - Equal"
    },
    {
      value: "NEQ",
      label: "NEQ - Not Equal"
    },
    {
      value: "0",
      label: "0 - None"
    },
    {
      value: "LE",
      label: "LE - Less or Equal"
    },
    {
      value: "LT",
      label: "LT - Less Than"
    },
  ]

  return <div className={classes.row} key={uuid()}>
    <label>{name}:</label>
    <Select
      key={uuid()}
      value={value}
      onChange={e => handleInputChange(e)}
      displayEmpty
      className={classNames({ input: true })}
    >
      {
        selectionTypes.map(tp => {
          return <MenuItem key={tp.value} value={tp.value}>{tp.label}</MenuItem>
        })
      }
    </Select>
  </div>
}

export default ElementInputDetail;