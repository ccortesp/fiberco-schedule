import React, { useState, useEffect, useRef } from 'react';
import {
  makeStyles,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  Accordion,
  Typography,
  AccordionSummary,
  AccordionDetails
} from '@material-ui/core';
import {
  blue,
  grey,
  darkBlue,
  lightBlue,
  lowGrey,
  midGrey
} from '../common/colors';
import { DevicesOther as DeviceIcon } from '@material-ui/icons';
import {
  Threshold,
  SimpleThresholdRule,
  SimpleThresholdRuleOption
} from '../common/Types';
import classNames from 'classnames';
import { setHours, setMinutes, getDay, getHours } from 'date-fns';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ElementInputDetail from './ElementInputDetail';
import ElementSelectDetail from './ElementSelectDetail';
import { v1 as uuid } from 'uuid';

const useStyles = makeStyles({
  cardContainer: {
    marginBottom: "10px"
  },
  card: {
    display: "flex",
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    "& .MuiExpansionPanelSummary-content": {
      alignItems: "center !important"
    }
  },
  itemName: {
    fontWeight: "bold",
    fontSize: "14px",
    width: "400px",
    display: "flex",
    alignItems: "center"
  },
  itemCount: {
    flexGrow: 1,
    display: "flex",
    fontSize: "14px",
    alignItems: "center"
  },
  icon: {
    color: blue,
    padding: "8px",
    width: "40px",
    height: "40px",
    justifyContent: "center",
    alignItems: "center",
    marginRight: "20px",
  },
  btn: {
    border: "none",
    borderRadius: "5px",
    color: "white",
    height: "30px",
    padding: "0px 15px",
    fontSize: "13px",
    cursor: "pointer",
    fontWeight: "bold",
    "&.btnPrimary": {
      backgroundColor: darkBlue,
      "&:hover": {
        backgroundColor: blue
      },
      "&:disabled,&[disabled]": {
        backgroundColor: midGrey
      }
    },
    "&.btnSecondary": {
      backgroundColor: grey,
      color: "white",
      "&:hover": {
        backgroundColor: grey,
      }
    }
  },
  options: {
    display: "flex",
    flexDirection: "row",
    "& button": {
      margin: "0 2px 0 2px"
    }
  },
  details: {
    flexDirection: "column",
    display: "flex",

  },
  availableTime: {
    color: "green"
  },
  noAvailableTime: {
    color: "red"
  },
  innerContainer: {
    display: "flex",
    flexDirection: "column",
    width: "350px"
  }
});

type Props = {
  item: Threshold,
  icon: DeviceIcon,
  onSave: func
}



const ThresholdCard = (props: Props) => {
  const { item, icon } = props;
  const classes = useStyles();



  const simpleThresholdRule = useRef({
    ...item.simpleThresholdRule
  })

  const handleElementChange = (name, value) => {

    simpleThresholdRule.current = {
      ...simpleThresholdRule.current,
      [name]: value
    }

  }

  const handleOnSave = () => {

    const threshold = {
      ...item,
      simpleThresholdRule: {
        ...item.simpleThresholdRule,
        ...simpleThresholdRule.current

      },
    };
    props.onSave(threshold);
  }


  return (
    <Accordion className={classes.cardContainer}>
      <AccordionSummary
        className={classes.card}
        expandIcon={<ExpandMoreIcon className={classes.icon} />}>

        <DeviceIcon className={classes.icon} />

        <span className={classes.itemName}>
          {item.thresholdRuleName}
        </span>


        <div className={classes.itemCount}>
          <div className={classes.innerContainer}>
            <span><b>Measurement:&nbsp;</b> {` ${item.measurement.name}`}</span>
            <span><b>Description:&nbsp;</b> {` ${item.measurement.description}`}</span>
          </div>
          <div className={classes.innerContainer}>
            <span><b>MeasurementType:&nbsp;</b> {` ${item.measurement.measurementType}`}</span>
            <span><b>MeasurementUnit:&nbsp;</b> {` ${item.measurement.measurementUnit}`}</span>
          </div>



        </div>


        <div className={classes.options}>
          <button
            onClick={e => handleOnSave()}
            className={classNames(classes.btn, { btnPrimary: true })}>
            Save
          </button>
        </div>
      </AccordionSummary>
      <AccordionDetails>
        <div className={classes.details}>

          <ElementInputDetail
            key={uuid()}
            onInputChange={handleElementChange}
            value={item.simpleThresholdRule.conformanceTargetUpper}
            name={'conformanceTargetUpper'} />

          <ElementInputDetail
            key={uuid()}
            onInputChange={handleElementChange}
            value={item.simpleThresholdRule.conformanceTargetLower}
            name={'conformanceTargetLower'} />

          <ElementSelectDetail
            key={uuid()}
            onInputChange={handleElementChange}
            value={item.simpleThresholdRule.conformanceComparatorUpper}
            name={'conformanceComparatorUpper'} />


          <ElementSelectDetail
            key={uuid()}
            onInputChange={handleElementChange}
            value={item.simpleThresholdRule.conformanceComparatorLower}
            name={'conformanceComparatorLower'} />

        </div>

      </AccordionDetails>
    </Accordion >
  );
}

export default ThresholdCard;