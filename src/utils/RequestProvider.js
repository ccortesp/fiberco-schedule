
import axios, { URL_TYPES, API_MANAGER } from '../common/axios';

export const createRequest = (
  type: URL_TYPES,
  data = null,
  params = null,
  successHandler = () => { },
  errorHandler = () => { }) => {

  const url = params === null
    ? API_MANAGER[type].url
    : buildParams(API_MANAGER[type].url, params)


  const config = {
    ...API_MANAGER[type],
    url,
    data
  };

  doRequest(config, successHandler, errorHandler);
}


export const buildParams = (url, params) => {
  let newUrl = url;
  Object.keys(params).forEach(key => {
    newUrl = newUrl.replace(`{${key}}`, params[key]);
  });



  return newUrl;

}

export const doRequest = (config, successHandler, errorHandler) => {
  axios({
    method: config.method,
    url: config.url,
    data: config.data
  })
    .then(successHandler)
    .catch(errorHandler);
}