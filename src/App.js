import React from "react";
import Sidebar from "./components/Sidebar";
import SidebarContextProvider from "./common/SidebarContext";
import { Switch, Route, Redirect } from "react-router-dom";
import { v4 as uuid } from "uuid";
import { makeStyles } from "@material-ui/core";
import { lowGrey, blue, lightBlue, grey } from "./common/colors";
import DataTreeNodeContextProvider from "./common/DataTreeNodeContext";
import { links } from "./common/routes";
import SchedulePage from "./pages/Schedule";

const useStyles = makeStyles({
  root: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  page: {
    display: "flex",
    height: "100vh",
    overflow: "auto",
    flexGrow: 1,
    overflowX: "hidden",
    flexDirection: "column",
    backgroundColor: lowGrey,
  },
  content: {
    padding: "24px",
    flexDirection: "column",
    display: "flex",
    overflow: "hidden",
  },
  divider: {
    border: "none",
    height: "1px",
    margin: 0,
    flexShrink: 0,
    backgroundColor: "rgba(0, 0, 0, 0.12)",
    "&.vertical": {
      width: "1px",
      height: "100%",
    },
    "&.horizontal": {
      width: "100%",
      height: "1px",
    }
  },
  btn: {
    border: "none",
    borderRadius: "5px",
    color: "white",
    height: "30px",
    padding: "0px 15px",
    fontSize: "13px",
    cursor: "pointer",
    fontWeight: "bold",
    "&.btnPrimary": {
      backgroundColor: blue,
      "&:hover": {
        backgroundC: lightBlue
      }
    },
    "&.btnSecondary": {
      backgroundColor: grey,
      color: "white",
      "&:hover": {
        backgroundColor: grey,
      }
    }
  }
});

function App() {


  const classes = useStyles();


  return <div className={classes.root}>
    <SidebarContextProvider>
      <Sidebar />

      <DataTreeNodeContextProvider>

        <Switch>
          <Route
            exact={true}
            path="/"
            component={() => <SchedulePage classes={classes} />} />

          {
            links.map(
              (link) => (
                <Route
                  key={uuid()}
                  exact={true}
                  path={link.path}
                  component={
                    () => <link.component
                      key={uuid()}
                      classes={classes}
                      index={link.index} />
                  }
                />
              )
            )
          }

          <Route path="*" component={() => <div>404</div>} />
          <Route path="/404" component={() => <div>404</div>} />
        </Switch>
      </DataTreeNodeContextProvider>

    </SidebarContextProvider>

  </div>
}

export default App;
