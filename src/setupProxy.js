const { createProxyMiddleware } = require('http-proxy-middleware');
module.exports = function (app) {
  app.use(
    '/tmf-api/workTicketManagement/*',
    createProxyMiddleware({
      target: 'http://35.192.1.200:8098',
      changeOrigin: false,
      secure: false
    })
  );
  app.use(
    '/tmf-api/649/*',
    createProxyMiddleware({
      target: 'http://35.202.58.165:8080',
      changeOrigin: false,
      secure: false,

    })
  )
};